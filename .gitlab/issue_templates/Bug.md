<!-- 
Please take a minute and check our wiki page which may include the
information you're looking for:
https://gitlab.com/jmiskinis/gnome-shell-extension-task-widget/-/wikis/home
-->

#### Bug summary

<!-- Provide a short summary of the bug you encountered. -->



#### Steps to reproduce

<!-- 
1. Step one
2. Step two
3. ...
-->

#### What happened

<!-- What did the extension do that was unexpected? -->



#### What did you expect to happen

<!-- What did you expect the extension to do? -->



#### Support log ([Instructions](https://gitlab.com/jmiskinis/gnome-shell-extension-task-widget/-/wikis/Help#support-log))

```
This extension has a "Support Log" feature which provides useful information
about your system and errors. Please refer to our wiki page (link above) to
learn how to use it and then replace this text with the output of the log. You
should remove or censor any information you consider private before submitting.
```

#### Other relevant logs, screenshots, screencasts etc.

<!-- 
If you have further information, such as technical documentation, logs,
screenshots or screencasts related, please provide them here.
-->



<!-- Do not remove the following line. -->
/label ~"1. Bug"